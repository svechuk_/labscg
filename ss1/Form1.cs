﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ss1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        void print(int x, Color color)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            SolidBrush ha = new SolidBrush(color);
            g.FillRectangle(ha, 400, 300 - x, 100, 180);

            Point[] pol =
            {
                new Point(400, 300 - x),
                new Point(500, 300 - x),
                new Point(450, 250 - x)
            };
            SolidBrush hb = new SolidBrush(color);
            g.FillPolygon(hb, pol);

            Point[] r1 =
            {
                new Point(400, 480 - x),
                new Point(400, 530 - x),
                new Point(433, 480 - x)
            };
            SolidBrush hd = new SolidBrush(color);
            g.FillPolygon(hd, r1);

            Point[] r2 =
            {
                new Point(433, 480 - x),
                new Point(449, 530 - x),
                new Point(466, 480 - x)
            };
            SolidBrush he = new SolidBrush(color);
            g.FillPolygon(he, r2);

            Point[] r3 =
            {
                new Point(466, 480 - x),
                new Point(500, 530 - x),
                new Point(500, 480 - x)
            };
            SolidBrush hf = new SolidBrush(color);
            g.FillPolygon(hf, r3);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            pictureBox2.Hide();
            int n = 0;
            print(n, Color.Tomato);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            pictureBox2.Hide();
            Thread a = new Thread(fun);
            a.Start();
        }
        void fun()
        {
            SolidBrush s = new SolidBrush(Color.White);
            int step = 0;

            for (int i = 0; i < 50; i++)
            {
                print(step, Color.Tomato);
                Thread.Sleep(100);
                print(step, Color.White);
                step += 10;
            }
                
        }

        private void button3_Click(object sender, EventArgs e)
        {
            pictureBox2.Hide();
            Form1_Resize(sender, e);
            pictureBox1.Show();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            one();
        }
        void one()
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen sv = new Pen(Color.Black,1);
            Pen eta = new Pen(Color.Green, 2);
            Pen sv1 = new Pen(Color.Red, 2);
            Font drawFont = new Font("Arial", 12);
            Font signatureFont = new Font("Arial", 7);

            SolidBrush drawBrush = new SolidBrush(Color.Blue);
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            int sizeWidth = Form1.ActiveForm.Width;
            int sizeHeight = Form1.ActiveForm.Height;
            Point center = new Point(((int)(sizeWidth/2)-8),(int)((sizeHeight/2)-8));

            //оси:

            g.DrawLine(sv, 10, center.Y,center.X,center.Y); //x-
            g.DrawLine(sv, center.X, center.Y, center.X * 2 - 10, center.Y); //x+
            g.DrawLine(sv, center.X, 10, center.X, center.Y); //y+
            g.DrawLine(sv, center.X, center.Y, center.X, center.Y * 2 - 10); //y-

            g.DrawString("X",drawFont,drawBrush, new PointF(center.X*2-5,center.Y+10),drawFormat); //подписываем ось Х
            g.DrawString("Y", drawFont, drawBrush, new PointF(center.X+20, 5),drawFormat); //подписываем ось У
            g.DrawString("0", drawFont, drawBrush, new PointF(center.X - 5, center.Y+5), drawFormat); //подпиываем начало координат

            g.DrawLine(sv,center.X-5,20,center.X,10); // 1я часть стрелки У
            g.DrawLine(sv, center.X + 5, 20, center.X, 10); // 2я часть стрелки У

            g.DrawLine(sv, center.X*2 - 20, center.Y-5, center.X*2-10, center.Y); // 1я часть стрелки Х
            g.DrawLine(sv, center.X * 2 - 20, center.Y + 5, center.X * 2 - 10, center.Y); // 2я часть стрелки Х

            int stepForAxes = 25; //расстояние м/д делениями
            int lShtrih = 3; //половина длины штриха
            int maxX = 4; // макс значение по Х
            int maxY = 4; //макс значение по У

            float oneDelenieX=(float)maxX/((float)center.X/(float)stepForAxes); //значение одного деления по Х
            float oneDelenieY = (float)maxY / ((float)center.Y / (float)stepForAxes); //значение одного деления по У

            //рисуем штрихи:
            //для Х:
            for (int i=center.X,j=center.X,k=1;i<center.X*2-30;j-=stepForAxes,i+=stepForAxes,k++)
            {
                g.DrawLine(sv,i,center.Y-lShtrih,i,center.Y+lShtrih); 
                g.DrawLine(sv, j, center.Y - lShtrih, j, center.Y + lShtrih);
                
                if (i<center.X*2-55)
                {
                    g.DrawString((k * oneDelenieX).ToString("0.0"), signatureFont, drawBrush, new PointF(i + stepForAxes + 9, center.Y + 6), drawFormat);
                    g.DrawString((k * oneDelenieX).ToString("0.0") + "-", signatureFont, drawBrush, new PointF(j - stepForAxes + 9, center.Y + 6), drawFormat);
                }
            }
            //для У:
            for (int i = center.Y, j = center.Y, k = 1; i < center.Y * 2 - 30; j -= stepForAxes, i += stepForAxes, k++)
            {
                g.DrawLine(sv, center.X - lShtrih, i, center.X + lShtrih, i);
                g.DrawLine(sv, center.X - lShtrih, j, center.X + lShtrih, j);

                if (i < center.Y * 2 - 55)
                {
                    g.DrawString((k * oneDelenieX).ToString("0.0") + "-", signatureFont, drawBrush, new PointF(center.X + 24, i + stepForAxes + 9), drawFormat);
                    g.DrawString((k * oneDelenieX).ToString("0.0"), signatureFont, drawBrush, new PointF(center.X + 24, j - stepForAxes - 9), drawFormat);
                }
            }

            //построение графика функции:
            //X>0

            int numOfPoint = 100;
            float[] first = new float[numOfPoint];
            for (int i = 0; i < numOfPoint; i++)
            {
                first[i] = (float)maxX / numOfPoint * (i + 1);
            }

            float[] second = new float[numOfPoint];
            for (int i = 0; i < numOfPoint; i++)
            {
                second[i] = -(float)(1 / (float)first[i]);
            }
            Point[] pointOne = new Point[numOfPoint];
            float tempX = 1 / oneDelenieX * stepForAxes;
            float tempY = 1 / oneDelenieY * stepForAxes;

            for (int i = 0; i < numOfPoint; i++)
            {
                pointOne[i].X = center.X + (int)(first[i] * tempX);
                pointOne[i].Y = center.Y + (int)(second[i] * tempY);
            }

            g.DrawLines(sv1, pointOne);
            g.DrawCurve(eta, pointOne);

            //X<0

            for (int i=0;i<numOfPoint;i++)
            {
                first[i] = ((float)maxX-(float)maxX*2) / numOfPoint * (i + 1);
            }
            
            for (int i = 0; i < numOfPoint; i++)
            {
                second[i] = -(float)(1 / (float)first[i]);
            }
      
            for (int i= 0;i< numOfPoint;i++)
            {
                pointOne[i].X = center.X + (int)(first[i]*tempX);
                pointOne[i].Y = center.Y + (int)(second[i] * tempY);
            }

            g.DrawLines(sv1,pointOne);
            g.DrawCurve(eta, pointOne);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            pictureBox2.Image = Image.FromFile(openFileDialog1.FileName);
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            pictureBox2.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            pictureBox2.Hide();
            Graphics g = this.CreateGraphics(); // создаем объект
            LinearGradientBrush myBrush = new LinearGradientBrush(ClientRectangle, Color.Black, Color.Blue,
                System.Drawing.Drawing2D.LinearGradientMode.Horizontal); //создаем кисть
            Font myFont = new Font("Tahoma", 24, FontStyle.Regular); // выбираем шрифт
            g.DrawString("Сулейманова Светлана", myFont, myBrush, new RectangleF(30,160,400,100)); 
        }

        private void button6_Click(object sender, EventArgs e)
        {
            
            Rectangle bounds = Bounds;
            Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height);
            Graphics g = Graphics.FromImage(bitmap);
            g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
            saveFileDialog1.ShowDialog();
            ImageFormat img = ImageFormat.Jpeg;
            switch (saveFileDialog1.FilterIndex)
            {
                case 0: img = ImageFormat.Jpeg; break;
                case 1: img = ImageFormat.Bmp; break;
                case 2: img = ImageFormat.Png; break;
            }
            bitmap.Save(saveFileDialog1.FileName, img);
        }

    }
}
